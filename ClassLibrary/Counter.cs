﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Counter
    {

        // Attribut de la classe Counter //
        // Valeur = 0 car le compteur commence à cette valeur
        private static int Valeur = 0;

        // GET && SET de la classe Counter
        public static int getValeur { get { return Valeur; } }
        public static void setValeur(int value) { Valeur = value; }

        // Méthodes de la classe Counter //
        public static int Incrementation()
        {
            Valeur++;
            return Valeur;
        }

        public static int Decrementation()
        {
            if(Valeur <= 0)
            {
                Valeur = 0;
                return Valeur;
            }
            else
            {
                Valeur--;
                return Valeur;
            }           
        }

        public static int RAZ()
        {
            Valeur = 0;
            return Valeur;
        }
    }
}
